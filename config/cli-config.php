<?php

/**
 * Configuration for doctrine database cli tools
 */

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

// using vendors autoload
require_once 'vendor/autoload.php';

// Bootstrap Doctrine 2 for cli
require_once "config/database.php";

$paths = array("src/Application/Model");
$isDevMode = true;

// the connection configuration
$dbParams = array(
    'driver' => 'pdo_mysql',
    'user' => $db_user,
    'password' => $db_password,
    'dbname' => $db_name,
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$entityManager = EntityManager::create($dbParams, $config);

return ConsoleRunner::createHelperSet($entityManager);