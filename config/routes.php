
<?php
/**
 * Routes settings
 */
$route_config = [
    'default' => ['path' => '/', 'controller' => 'DefaultController:index'],
    'registration' => ['path' => '/registration', 'controller' => 'RegistrationController:registration'],
    'welcome' => ['path' => '/welcome', 'controller' => 'RegistrationController:welcome'],
    'login' => ['path' => '/login', 'controller' => 'SecurityController:login'],
    'logout' => ['path' => '/logout', 'controller' => 'SecurityController:logout'],
    'profile' => ['path' => '/profile', 'controller' => 'UserController:profile'],
    'change_password' => ['path' => '/change-password', 'controller' => 'UserController:changePassword'],
    'filelist' => ['path' => '/files', 'controller' => 'FileController:index'],
    'download_file' => ['path' => '/file/{id}', 'controller' => 'FileController:download', 'parameters' => ['id' => '\d+']],
    'remove_file' => ['path' => '/file/remove/{id}', 'controller' => 'FileController:remove', 'parameters' => ['id' => '\d+']],
    'error' => ['path' => '/error', 'controller' => 'DefaultController:error'],
];
