<?php
/**
 * Bootstrap additional parts of framework
 */

require_once "../vendor/autoload.php";

use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Application\Core\Context;

///////////////////////
// Bootstrap Doctrine 2
///////////////////////
require_once "../config/database.php";

$paths = array("../src/Application/Model");
$isDevMode = true;

// the connection configuration
$dbParams = array(
    'driver' => 'pdo_mysql',
    'user' => $db_user,
    'password' => $db_password,
    'dbname' => $db_name,
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$entityManager = EntityManager::create($dbParams, $config);

// Save configured entity manager to a singleton with settings
Context::getInstance()->em = $entityManager;

////////////////////
// Bootstrap routes
////////////////////
require_once "../config/routes.php";

$routes = new RouteCollection();

/**
 * See $route_config in config/routes.php
 */
foreach ($route_config as $name => $route) {
    $parameters = (empty($route['parameters'])) ? [] : $route['parameters'];
    $routes->add($name,
        new Route(
            $route['path'],
            ['controller' => $route['controller']],
            $parameters
        )
    );
}

$context = new RequestContext($_SERVER['REQUEST_URI']);
$request = Request::createFromGlobals();
$matcher = new UrlMatcher($routes, $context);

// Save configured matcher to a singleton with settings
Context::getInstance()->matcher = $matcher;

//////////////////
// Bootstrap twig
//////////////////
$auto_reload = true;
$cache_dir = '../cache';
$loader = new Twig_Loader_Filesystem('../src/Application/View');
$twig = new Twig_Environment($loader, array(
    'cache' => $cache_dir,
    'auto_reload' => $auto_reload
));

// Save configured twig to a singleton with settings
Context::getInstance()->twig = $twig;

////////////////////////
// Bootstrap upload dir
////////////////////////
$upload_path = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . "uploads";
if (!file_exists($upload_path)) {
    mkdir($upload_path, 0777, true);
}
Context::getInstance()->upload_path = $upload_path;
