<?php

namespace Application\Core;

use Application\Core\Context;

class Controller
{
    public $context = null;

    public function __construct()
    {
        $this->context = Context::getInstance();
        $this->context->authorized = empty($_SESSION['authorized']) ? null : $_SESSION['authorized'];
    }
}
