<?php
/**
 * Context is singleton for store application
 * settings after bootstrap
 */
namespace Application\Core;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Matcher\UrlMatcher;

class Context
{

    protected static $instance;

    /**
     * @var UrlMatcher
     */
    public $matcher;

    /**
     * @var EntityManager
     */
    public $em;

    /**
     * @var \Twig_Environment
     */
    public $twig;

    /**
     * @var boolean
     */
    public $authorized = false;

    /**
     * Path to upload dir
     * @var string
     */
    public $upload_dir = null;

    private function __construct()
    {

    }

    private function __clone()
    {
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getUser()
    {
        return $this->em->getRepository('Application\Model\User')->find($_SESSION['user_id']);
    }
}
