<?php

namespace Application\Model;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Application\Model\Log;

/**
 * @Entity()
 * @HasLifecycleCallbacks
 */
class User
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @Column(type="string", length=255)
     */
    private $login;

    /**
     * @Column(type="string", length=255)
     */
    private $password;

    /**
     * @Column(type="string", length=255)
     */
    private $first_name;

    /**
     * @Column(type="string", length=255)
     */
    private $last_name;

    /**
     * @Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @Column(type="date", nullable=true)
     */
    private $birthday;

    /**
     * @OneToMany(targetEntity="File", mappedBy="user")
     **/
    private $files;

    /**
     * @OneToMany(targetEntity="Log", mappedBy="user")
     **/
    private $logs;

    /**
     * @Column(type="datetime")
     */
    private $created_at;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
        $this->logs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->created_at = new \DateTime();
    }

    /** @PostUpdate */
    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();
        $uow->computeChangeSets(); // do not compute changes if inside a listener
        $user = $eventArgs->getEntity();
        $changeset = $uow->getEntityChangeSet($user);

        foreach ($changeset as $field => $info) {
            if ($field === 'birthday') {
                $old_value = $info[0]->format("Y-m-d");
                $new_value = $info[1]->format("Y-m-d");
                if ($old_date === $new_date) {
                    continue;
                }
            }
            else {
                $old_value = $info[0];
                $new_value = $info[1];
            }
            $updated_at = new \DateTime;

            $conn = $em->getConnection();
            $conn->insert('log',
                [
                    'user_id' => $user->getId(),
                    'field' => $field,
                    'oldValue' => $old_value,
                    'newValue' => $new_value,
                    'updated_at' => $updated_at->format('Y-m-d H:i:s')
                ]
            );
        }
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set login
     *
     * @param string $login
     * @return User
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get first_name
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Add files
     *
     * @param \Application\Model\File $files
     * @return User
     */
    public function addFile(\Application\Model\File $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \Application\Model\File $files
     */
    public function removeFile(\Application\Model\File $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Add logs
     *
     * @param \Application\Model\Log $logs
     * @return User
     */
    public function addLog(\Application\Model\Log $logs)
    {
        $this->logs[] = $logs;

        return $this;
    }

    /**
     * Remove logs
     *
     * @param \Application\Model\Log $logs
     */
    public function removeLog(\Application\Model\Log $logs)
    {
        $this->logs->removeElement($logs);
    }

    /**
     * Get logs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogs()
    {
        return $this->logs;
    }
}
