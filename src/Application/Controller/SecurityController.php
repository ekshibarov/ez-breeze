<?php

use Application\Core\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SecurityController extends Controller 
{
    public function login()
    {
    	$login = (empty($_POST['login'])) ? null : $_POST['login'];
    	$password = (empty($_POST['password'])) ? null : $_POST['password'];
    	$user = $this->context->em->getRepository('Application\Model\User')->findOneBy(['login' => $login]);
    	if (password_verify($password, $user->getPassword())) {
    		if (session_status() !== PHP_SESSION_NONE) {
    			session_start();
    		}
    		$_SESSION["authorized"] = true;
    		$_SESSION["user_id"] = $user->getId();
	        $redirection = new RedirectResponse("/profile");
	        $redirection->send();
	        return;
    	}
        echo $this->context->twig->render('default.html.twig');
    }

    public function logout()
    {
    	unset($_SESSION['authorized']);
    	unset($_SESSION['user_id']);
        $redirection = new RedirectResponse("/");
        $redirection->send();
        return;
    }
}