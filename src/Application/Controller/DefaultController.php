<?php

use Application\Model\User;
use Application\Core\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends Controller
{
    public function index()
    {
        $user = new User();
        if ($this->context->authorized) {
	        $redirection = new RedirectResponse("/profile");
	        $redirection->send();
	        return;
        }
        echo $this->context->twig->render('default.html.twig');
    }

    public function error()
    {
        echo $this->context->twig->render('error.html.twig', ['error_message' => $_SESSION['error_message']]);
    }
}
