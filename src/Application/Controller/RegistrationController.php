<?php

use Application\Core\Controller;
use Application\Model\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RegistrationController extends Controller
{
    private $required_fields = ['login', 'password', 'first_name', 'surname'];

    public function registration()
    {
        if ($this->validate($_POST)) {
            if (!$this->checkUser($_POST['login'])) {
                $this->createUser($_POST);
                $redirection = new RedirectResponse("/welcome");
                $redirection->send();
                return;
            }
        }
        else {
        }
        echo $this->context->twig->render('registration.html.twig');
    }

    public function welcome()
    {
        echo $this->context->twig->render('welcome.html.twig');
    }

    private function validate($post)
    {
        $invalid_fields = $this->getEmptyFields($post);
        if ($invalid_fields) {
            return false;
        }
        return true;
    }

    private function getEmptyFields($post)
    {
        $invalid_fields = null;
        foreach ($this->required_fields as $required) {
            if (!array_key_exists($required, $post)) {
                $invalid_fields[] = $required;
            }
        }
        return $invalid_fields;
    }

    private function checkUser($login)
    {
        $user = $this->context->em->getRepository('Application\Model\User')->findOneBy(['login' => $login]);
        if (!$user) {
            return false;
        }
        return true;
    }


    private function createUser($data)
    {
        $user = new User();
        $user->setLogin($data['login']);
        $user->setPassword($data['password']);
        $user->setFirstName($data['first_name']);
        $user->setLastName($data['surname']);

        $email = (empty($data['email'])) ? null : $data['email'];
        $user->setEmail($email);

        $phone = (empty($data['phone'])) ? null : $data['phone'];
        $user->setPhone($phone);

        $birthday = (empty($data['birthday'])) ? null : strtotime($data['birthday']);
        $birthday = new DateTime(date("Y-m-d h:i:s", $birthday));
        $user->setBirthday($birthday);

        $this->context->em->persist($user);
        $this->context->em->flush();
    }
}
