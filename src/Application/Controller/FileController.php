<?php

use Application\Core\Controller;
use Application\Model\File;
use Symfony\Component\HttpFoundation\RedirectResponse;

class FileController extends Controller
{

    public function index()
    {
        if (!empty($_FILES)) {
            if ($this->validate($_FILES['file'])) {
                $this->addFile($_FILES['file']);
            }
        }
        $files = $this->fetchCurrentUserFiles();

        echo $this->context->twig->render('filelist.html.twig', ['files' => $files]);
    }

    public function download($id)
    {
        $file = $this->context->em->getRepository('Application\Model\File')->findOneBy(
            [
                'id' => $id,
                'user' => $this->context->getUser(),
                'is_removed' => false,
            ]
        );
        if ($file) {
            ob_end_clean();
            header('Content-Description: File Transfer');
            header('Content-Type: ' . $file->getType());
            header('Content-Disposition: attachment; filename=' . basename($file->getName()));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file->getPath()));
            readfile($file->getPath());
        }
        else {
            $_SESSION['error_message'] = 'Файл уже удален или у вас не хватает прав на удаление.';
            $redirection = new RedirectResponse("/error");
	        $redirection->send();
	        return;
        }
    }

    public function remove($id)
    {
        $file = $this->context->em->getRepository('Application\Model\File')->findOneBy(
            [
                'id' => $id,
                'user' => $this->context->getUser(),
                'is_removed' => false,
            ]
        );
        if ($file) {
            unlink($file->getPath());
            $file->setIsRemoved(true);
            $this->context->em->persist($file);
            $this->context->em->flush();
            $redirection = new RedirectResponse("/files");
	        $redirection->send();
        }
        else {
            $_SESSION['error_message'] = 'Файл уже удален или у вас не хватает прав на удаление.';
            $redirection = new RedirectResponse("/error");
	        $redirection->send();
	        return;
        }
    }

    private function addFile($receivedFile)
    {
        $uploadfile = $this->context->upload_path . DIRECTORY_SEPARATOR . time() . "_" .  basename($receivedFile['name']);

        if (move_uploaded_file($receivedFile['tmp_name'], $uploadfile)) {
            $file = new File();
            $file->setName($receivedFile['name']);
            $file->setPath($uploadfile);
            $file->setSize($receivedFile['size']);
            $file->setType($receivedFile['type']);
            $file->setUser($this->context->getUser());

            $this->context->em->persist($file);
            $this->context->em->flush();
        }
    }

    private function validate($file)
    {
        if ($file['size'] > '1000000') {
            return false;
        }
        if (count($this->fetchCurrentUserFiles()) > 20) {
            return false;
        }
        return true;
    }

    private function fetchCurrentUserFiles()
    {
        $files = $this->context->em->getRepository('Application\Model\File')->findBy(
            [
                'user' => $this->context->getUser(),
                'is_removed' => false,
            ]
        );
        return $files;
    }
}
