<?php

use Application\Core\Controller;

class UserController extends Controller
{

	private $required_fields = ['first_name', 'surname'];

	public function profile()
	{
		if ($this->context->authorized) {
			$login = $this->context->getUser()->getLogin();
			$user = $this->context->em->getRepository('Application\Model\User')->findOneBy(['login' => $login]);

			if ($this->validate($_POST)) {
				$user = $this->updateProfile($_POST);
			}

			echo $this->context->twig->render('profile.html.twig', ['user' => $user]);
		}
		else {
			$_SESSION['error_message'] = 'Пользовательне авторизован';
            $redirection = new RedirectResponse("/error");
	        $redirection->send();
	        return;
		}
	}

	public function changePassword()
	{
		if (!empty($_POST) && !empty($_POST['old_password'])
			&& !empty($_POST['new_password']) && !empty($_POST['repeat_new_password'])) {
				$old_password = $_POST['old_password'];
				$new_password = $_POST['new_password'];
				$repeat_new_password = $_POST['repeat_new_password'];
				$user = $this->context->getUser();
				if (password_verify($old_password, $user->getPassword())) {
					if ($new_password == $repeat_new_password) {
						$user->setPassword($new_password);
						$this->context->em->persist($user);
						$this->context->em->flush();
					}
					else {
						$_SESSION['error_message'] = 'Новые пароли не совпадают';
			            $redirection = new RedirectResponse("/error");
				        $redirection->send();
				        return;
					}
				}
				else {
					$_SESSION['error_message'] = 'Неправильный пароль';
					$redirection = new RedirectResponse("/error");
					$redirection->send();
					return;
				}
		}
		echo $this->context->twig->render('change_password.html.twig');
	}

	/**
	 * Update current user with POST data
	 * @param array $data Updated data
	 */
	private function updateProfile($data)
	{
		$user = $this->context->getUser();
		$user->setFirstName($data['first_name']);
        $user->setLastName($data['surname']);

        $email = (empty($data['email'])) ? null : $data['email'];
        $user->setEmail($email);

        $phone = (empty($data['phone'])) ? null : $data['phone'];
        $user->setPhone($phone);

        $birthday = (empty($data['birthday'])) ? null : strtotime($data['birthday']);
        $birthday = new DateTime(date("Y-m-d h:i:s", $birthday));
        $user->setBirthday($birthday);

        $this->context->em->persist($user);
        $this->context->em->flush();

		return $user;
	}


	private function validate($post)
    {
        $invalid_fields = $this->getEmptyFields($post);
        if ($invalid_fields) {
            return false;
        }
        return true;
    }

    private function getEmptyFields($post)
    {
        $invalid_fields = null;
        foreach ($this->required_fields as $required) {
            if (!array_key_exists($required, $post)) {
                $invalid_fields[] = $required;
            }
        }
        return $invalid_fields;
    }

}
