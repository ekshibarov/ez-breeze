<?php

session_start();

require_once "../src/Application/Bootstrap.php";

use Application\Controller;


// TODO: Move request to context
$parameters = $matcher->match($request->getPathInfo());

$path = mb_split(":", $parameters['controller']);

// Fetch directory path
$directory_path = array_slice($path, 0, count($path) - 1);

// Fetch method
$method = $path[count($path) - 1];

// Fetch controller name
$controller = $path[count($path) - 2];

// Prepare path for check file
$real_path = ".."
    . DIRECTORY_SEPARATOR . "src"
    . DIRECTORY_SEPARATOR . "Application"
    . DIRECTORY_SEPARATOR . "Controller"
    . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $directory_path) . ".php";

// If file exist
if (file_exists($real_path)) {

    // Then try to find method
    require_once $real_path;
    $class_methods = get_class_methods($controller);

    // If method found, then
    if (in_array($method, $class_methods)) {
        $controller = new $controller;

        // call method

        if (array_key_exists('id', $parameters)) {
            call_user_func([$controller, $method], $parameters['id']);
        }
        else {
            call_user_func([$controller, $method]);
        }
    } else {
        echo "Controller $path not found";
        exit(-1);
    }
}
