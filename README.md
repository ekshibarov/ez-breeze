# Description
This is a test including registration, login, profile settings, file uploading/removing.

# Install
1. Clone git repo.
2. Run: `composer install`
2. Set up database settings in config/database.php.
3. Create database using MySQL client.
4. Run under application directory: `bin/doctrine orm:schema-tool:update --force`
5. Configure Apache for use 'web' folder or run `php -S localhost:8000` under 'web'.
6. Enable mod_rewrite for Apache, if use Apache